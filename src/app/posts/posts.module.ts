import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostListComponent } from './post-list/post-list.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { PostsRoutingModule } from './posts-routing.module';
import { PostService } from './post.service';

@NgModule({
  declarations: [PostListComponent, PostDetailComponent],
  imports: [CommonModule, HttpClientModule, PostsRoutingModule],
  providers: [PostService],
})
export class PostsModule {}
