import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from './post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private _baseUrl = 'https://jsonplaceholder.typicode.com/posts';
  constructor(private httpClient: HttpClient) { }

  public getPosts(): Observable<Post[]> {
    return this.httpClient.get(this._baseUrl) as Observable<Post[]>;
  }
  
  public getPostDetail(postId: string): Observable<Post> {
    return this.httpClient.get(`${this._baseUrl}/${postId}`) as Observable<Post>;
  }
}
