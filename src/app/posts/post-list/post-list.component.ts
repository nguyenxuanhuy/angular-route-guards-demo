import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { PostService } from '../post.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
})
export class PostListComponent implements OnInit {
  selectedId: number;

  posts$ = this.route.paramMap.pipe(
    switchMap((params) => {
      // (+) before `params.get()` turns the string into a number
      this.selectedId = +params.get('id');
      return this.postService.getPosts();
    })
  );

  constructor(
    private postService: PostService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {}
}
