import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { PostService } from '../post.service';
import { Post } from '../post.model';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
})
export class PostDetailComponent {
  post$ = this.route.paramMap.pipe(
    switchMap((params: ParamMap) =>
      this.postService.getPostDetail(params.get('id'))
    )
  );

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private router: Router
  ) {}

  gotoPostList(post: Post) {
    const postId = post ? post.id : null;
    this.router.navigate(['/posts', { id: postId }]);
  }
}
